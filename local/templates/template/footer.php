<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)die();?>
<footer>
  <div class="footer--main container">
    <div class="footer__logo"><a href=""><img class="footer__logo--img" src="<?=SITE_TEMPLATE_PATH?>/img/svg/logo.png"></a></div>
    <div class="footer__menu">
      <ul class="footer__main--01 footer__main--list">
        <li><a href="">Главная</a></li>
        <li><a href="">Каталог</a></li>
        <li><a href="">О компании</a></li>
      </ul>
      <ul class="footer__main--02 footer__main--list">
        <li><a href="">Оплата</a></li>
        <li><a href="">Доставка</a></li>
        <li><a href="">Контакты</a></li>
      </ul>
    </div>
    <div class="footer__phone">
      <p class="footer__phone--number"><a href="">8 800 800 00 00</a></p>
      <button class="footer__phone--feedback">Обратный звонок</button>
      <p class="footer__phone--email"><a href="">info@rctmsc.com</a></p>
    </div>
  </div>
  <div class="footer__whiteline container"></div>
  <div class="footer__social container">
    <div class="footer__social--buttons">
      <h2 class="footer__social--title">Присоединяйтесь к РКТ в социальных сетях</h2>
      <ul class="footer__social--button__list">
        <li class="footer__social--start"><a href="">ВК</a></li>
        <li><a href="">ФБ</a></li>
        <li><a href="">ИН</a></li>
        <li><a href="">ТВ</a></li>
        <li><a href="">ЮТ</a></li>
      </ul>
    </div>
    <div class="footer__social--container">
      <h2 class="footer__social--title">Подписывайтесь на нашу рассылку и получайте информацию о новинках,
        скидках и
        акциях</h2>

      <div class="email-subscription">
        <form class="email__subscription--form" method="POST">
          <input type="email" placeholder="Введите ваш e-mail" />
          <input class="email-subscription--submit" type="submit" value="→">
        </form>
      </div>
    </div>
    <div class="footer__pay">
      <ul class="footer__pay--list">
        <li><a href="">VISA</a></li>
        <li><a href="">Master Card</a></li>
        <li><a href="">МИР</a></li>
        <li><a href="">Я.Деньги</a></li>
      </ul>
    </div>
  </div>
  <div class="footer__info container">
    <p class="footer__info--security">© 2018 РКТ. Все права защищены.</p>
    <p class="footer__info--confidentiality"><a href="">Политика конфиденциальности</a></p>
    <p class="footer__info--sitecreation"><a href="https://www.waytostart.ru">Создание сайта <span class="footer__info-sitecreation--span">WAYTOSTART</span></a></p>
  </div>

</footer>
</html>